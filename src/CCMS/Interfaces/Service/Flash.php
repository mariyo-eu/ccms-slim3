<?php

namespace CCMS\Interfaces\Service;

Interface Flash {

	/**
	 * Create new Flash messages service provider
	 *
	 * @param null|array|ArrayAccess $storage
	 * @throws RuntimeException if the session cannot be found
	 * @throws InvalidArgumentException if the store is not array-like
	 */
	public function __construct(&$storage = null);

	/**
	 * Add flash message for the next request
	 *
	 * @param string $key The key to store the message under
	 * @param mixed  $message Message to show on next request
	 */
	public function addMessage($key, $message);

	/**
	 * Add flash message for current request
	 *
	 * @param string $key The key to store the message under
	 * @param mixed  $message Message to show on next request
	 */
	public function addMessageNow($key, $message);

	/**
	 * Get flash messages
	 *
	 * @return array Messages to show for current request
	 */
	public function getMessages();

	/**
	 * Get Flash Message
	 *
	 * @param string $key The key to get the message from
	 * @return mixed|null Returns the message
	 */
	public function getMessage($key);

	/**
	 * Has Flash Message
	 *
	 * @param string $key The key to get the message from
	 * @return bool Whether the message is set or not
	 */
	public function hasMessage($key);
}
