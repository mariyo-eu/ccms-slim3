<?php

namespace CCMS\Services\Factory;

use CCMS\Services\Factory;
use CCMS\Interfaces;
use ReflectionClass;
use ReflectionException;
use ReflectionProperty;

/**
 * Models Factory.
 */
class Model extends Factory implements Interfaces\Service\Factory\Model
{

    /**
     * @param string $class
     * @param mixed $row
     * @return Interfaces\Model
     * @throws ReflectionException
     */
    public function create($class, $row = array())
    {
        // Create new Model instance
        $model = parent::create($class);

        if (empty($model)) {
            return NULL;
        }

        // Load reflection
        $reflection = new ReflectionClass($class);

        // Init submodels
        if (!isset($this->classes[$class]['submodels'])) {
            $this->classes[$class]['submodels'] = array();
        }

        // Is class a Model Item, unset it to be able __get properties?
        if ($reflection->implementsInterface(Interfaces\Model\Item::class)) {
            $properties = $reflection->getProperties();
            foreach ($reflection->getTraits() as $trait) {
                $properties += $trait->getProperties();
            }

            foreach ($properties as $property) {
                /* @var $property ReflectionProperty */
                if (!$property->isPublic()) {
                    continue;
                }

                if (isset($model->{$property->name})) {
                    continue;
                }

                // Load @var annotation
                $varAnnotation = array();
                preg_match('~@var\s+([\w\\\]+)~sm', $property->getDocComment(), $varAnnotation);

                if (empty($varAnnotation[1])) {
                    continue;
                }

                // Prepare class name from @var annotation
                $propertyClass = trim((strpos($varAnnotation[1], '\\') === 0 ? '' : $reflection->getNamespaceName()) . '\\' . $varAnnotation[1], '\\');

                if (!class_exists($propertyClass)) {
                    continue;
                }

                // Load property reflection
                $propertyClassReflection = new \ReflectionClass($propertyClass);

                // Is property really property
                if ($propertyClassReflection->implementsInterface(Interfaces\Model\Item\Property::class)) {
                    $this->classes[$class]['submodels'][] = $property;
                }
            }
        }

        // Unset submodel property
        foreach ($this->classes[$class]['submodels'] as $submodel) {
            unset($model->{$submodel->name});
        }

        if (!empty($row)) {
            $model->set($row);
        }

        return $model;
    }

}
