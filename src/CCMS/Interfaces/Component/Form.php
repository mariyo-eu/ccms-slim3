<?php

namespace CCMS\Interfaces\Component;

Interface Form extends \CCMS\Interfaces\Translatable {

	public function getForm();
}
