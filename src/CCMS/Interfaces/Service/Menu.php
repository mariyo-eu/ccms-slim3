<?php

namespace CCMS\Interfaces\Service;

use CCMS\Interfaces\Service;

Interface Menu extends Service {

	/**
	 * @param string $menu
	 * @return string
	 */
	public function render($menu);

	/**
	 * @param array $item
	 * @return string
	 */
	public function renderItem($item);
	
}
