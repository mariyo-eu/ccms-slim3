<?php

namespace CCMS\Interfaces\Service\Factory;

use CCMS\Interfaces\Service\Factory;

/**
 * Actions Factory.
 */
Interface Action extends Factory {
	
}
