<?php

namespace CCMS;

use CCMS\Interfaces;
use CCMS\Services;

if (!function_exists('registerConfig')) {

    /**
     * Loads config files:
     * - first look into folders: config-local, config
     * - than look in environment variable called site (optional)
     * - than look in environment variable called environment (optional)
     *
     * @param string                     $fileName
     * @param \CCMS\Interfaces\Container $ci
     *
     * @return array
     */
    function registerConfig($fileName, Interfaces\Container $ci = null)
    {
        $path = getConfigPath($fileName);
        if ($path) {
            return require $path;
        }

        return [];
    }

}

if (!function_exists('getConfigPath')) {

    /**
     * Check config files:
     * - first look into folders: config-local, config
     * - than look in environment variable called site (optional)
     * - than look in environment variable called environment (optional)
     *
     * @param string $fileName
     *
     * @return string
     */
    function getConfigPath($fileName)
    {
        $protectedDir = defined('PROTECTED_DIR') ? PROTECTED_DIR : realpath('./');

        foreach ([getenv('site'), ''] as $sitePath) {
            foreach ([getenv('environment'), ''] as $envPath) {
                foreach (['config-local', 'config'] as $configPath) {
                    $path = realpath($protectedDir . '/' . $configPath . '/' . $sitePath . '/' . $envPath . '/' . $fileName);
                    if ($path) {
                        return $path;
                    }
                }
            }
        }

        return null;
    }

}