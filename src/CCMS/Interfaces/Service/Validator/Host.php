<?php

namespace CCMS\Interfaces\Service\Validator;

use CCMS\Interfaces\Service;
use Psr\Http\Message\RequestInterface;

interface Host extends Service
{

    public function isValidHost(RequestInterface $request, string $routeName): bool;

    public function getValidUrl(RequestInterface $request, string $routeName): string;

}
