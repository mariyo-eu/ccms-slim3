<?php

namespace CCMS\Services;

use CCMS\Interfaces;

/**
 * Translator service.
 */
class Translator extends Base implements Interfaces\Service\Translator {

	/**
	 * @param   string   $string  text to translate
	 * @param   string   $context context of string
	 * @param   array    $values  values to replace in the translated text
	 * @param   int|bool $count   number for plural form
	 * @return string
	 */
	public function _t($string, $context = NULL, array $values = NULL, $count = FALSE) {
		// Default context
		if ($context === NULL) {
			$context = 'default';
		}

		// Fallback
		$translation = $string;

		// Add values to translation
		return empty($values) ? $translation : strtr($translation, $values);
	}

}
