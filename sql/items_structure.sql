-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Hostiteľ: localhost:3306
-- Čas generovania: Pi 29.Sep 2017, 09:50
-- Verzia serveru: 10.1.26-MariaDB-0+deb9u1
-- Verzia PHP: 7.0.19-1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Databáza: `esopa`
--

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `items`
--
-- Vytvorené: St 27.Sep 2017, 08:28
--

CREATE TABLE IF NOT EXISTS `items` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `id-type` (`id`,`type`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `items_values`
--
-- Vytvorené: St 27.Sep 2017, 10:12
--

CREATE TABLE IF NOT EXISTS `items_values` (
  `items_id` bigint(20) UNSIGNED NOT NULL COMMENT 'ID from items table',
  `related_id` bigint(20) UNSIGNED DEFAULT NULL COMMENT 'ID of item, when there is relation between items',
  `relation` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Type of relation',
  `lang` varchar(5) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Language of property or relation',
  `value` longtext CHARACTER SET utf8 COMMENT 'Attribute value',
  UNIQUE KEY `unique_index` (`items_id`,`relation`,`related_id`,`lang`) USING BTREE,
  KEY `Delete value when deleted related item` (`related_id`),
  KEY `relation-value` (`relation`,`value`(255))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Kľúče pre exportované tabuľky
--

--
-- Indexy pre tabuľku `items_values`
--
ALTER TABLE `items_values` ADD FULLTEXT KEY `value` (`value`);

--
-- Obmedzenie pre exportované tabuľky
--

--
-- Obmedzenie pre tabuľku `items_values`
-- CONSTRAINT - meno musi byt unique!
--
ALTER TABLE `items_values`
  ADD CONSTRAINT `Delete values when deleted items` FOREIGN KEY (`items_id`) REFERENCES `items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Delete values when deleted related items` FOREIGN KEY (`related_id`) REFERENCES `items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

COMMIT;