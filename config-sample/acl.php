<?php

use CCMS\Actions;

return [
	Actions\Welcome::class => ['login', 'superadmin'],
	Actions\Admin\Dashboard::class => ['superadmin']
];
