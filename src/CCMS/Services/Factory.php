<?php

namespace CCMS\Services;

use CCMS\Interfaces;

/**
 * Actions Factory.
 */
class Factory extends Base implements Interfaces\Service\Factory
{

    /**
     * @var Interfaces\Application
     */
    protected $app;

    /**
     * @var Interfaces\Container
     */
    protected $ci;

    /**
     * @var array
     */
    protected $classes;

    /**
     * @param Interfaces\Container $ci
     */
    public function __construct(Interfaces\Container $ci)
    {
        $this->ci  = $ci;
    }

    /**
     * @param string $class
     * @param array  $params
     *
     * @return object
     * @deprecated
     */
    public function create($class, $params = [])
    {
        return $this->ci->create($class, $params);
    }
}
