<?php

use Slim\App;
use CCMS\Actions;
use CCMS\Interfaces;
use CCMS\Services\Acl;
use CCMS\Services\Factory\Action;

/* @var $app App */
/* @var $actionFactory Action */
$actionFactory = $app
		->getContainer()
		->get(Interfaces\Service\Factory\Action::class);

// Default Group
$app->group('', function() use($actionFactory) {

	// Welcome action
	$this->any('/', $actionFactory->create(Actions\Welcome::class))->setName(Actions\Welcome::class);

	// Login action
	$this->any('/login', $actionFactory->create(Actions\Login::class))->setName(Acl::DEFAULT_ROUTE);

	// Admin Group
	$this->group('/admin', function() use($actionFactory) {

		// Admin Dashboard
		$this->any('', $actionFactory->create(Actions\Admin\Dashboard::class))->setName(Actions\Admin\Dashboard::class);
	});
});
