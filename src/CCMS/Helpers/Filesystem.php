<?php

namespace CCMS\Helpers;

abstract class Filesystem {

	/**
	 * @param type $target
	 * @param type $contents
	 */
	public static function fileForceContents($target, $contents) {
		$parts = preg_split('~[/\\\]~', $target);
		$file = array_pop($parts);
		$dir = realpath(array_shift($parts));

		foreach ($parts as $part) {
			if (!is_dir($dir .= "/$part")) {
				mkdir($dir);
			}
		}

		file_put_contents("$dir/$file", $contents);
	}

	/**
	 * @param string $url
	 * @return string
	 */
	public static function curlGetContents($url) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
		$data = curl_exec($ch);
		curl_close($ch);
		return $data;
	}

}
