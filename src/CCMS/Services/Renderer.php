<?php

namespace CCMS\Services;

use CCMS\Interfaces;
use Slim\Views\PhpRenderer;

/**
 * Service provides renderer functionality.
 */
class Renderer extends PhpRenderer implements Interfaces\Renderer {

}
