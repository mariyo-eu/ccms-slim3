<?php

namespace CCMS\Interfaces;

Interface Action extends Translatable {

	/**
	 * Content of Action.
	 */
	public function run();
}
