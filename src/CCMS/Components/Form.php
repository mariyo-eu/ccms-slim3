<?php

namespace CCMS\Components;

use CCMS\Interfaces;

/**
 * Abstract base form.
 */
abstract class Form implements Interfaces\Component\Form {

	/**
	 * @var \CCMS\Interfaces\Service\Translator
	 * @autowire
	 */
	protected $translator;

	/**
	 * @var \CCMS\Interfaces\Renderer
	 * @autowire
	 */
	protected $renderer;

	/**
	 * @var string 
	 */
	protected $layout;

	/**
	 * @param   string  $string		text to translate
	 * @param   array   $values		values to replace in the translated text
	 * @param   int		$count		number for plural form
	 * @return  string
	 */
	public function _t($string, array $values = NULL, $count = FALSE) {
		// Context
		$context = str_replace('\\', '/', strtolower(get_called_class()));

		// Translate
		return $this->translator->_t($string, $context, $values, $count);
	}

	/**
	 * @return string
	 */
	public function __toString() {
		return $this
						->renderer
						->fetch($this->layout, ['form' => $this->getForm(), 'component' => $this]);
	}

}
