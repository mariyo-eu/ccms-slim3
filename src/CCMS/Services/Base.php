<?php

namespace CCMS\Services;

use CCMS\Interfaces;

/**
 * Abstract Base Service.
 */
abstract class Base implements Interfaces\Service {

	/**
	 * @return $this
	 */
	public function __invoke() {
		return $this;
	}

}
