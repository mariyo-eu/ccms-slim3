<?php

namespace CCMS\Services;

use CCMS\Interfaces\Service;

/**
 * Service provides Menu.
 */
class Menu extends Base implements Service\Menu {

	/**
	 * @var \CCMS\Interfaces\Service\Config
	 * @autowire
	 */
	protected $config;

	/**
	 * @var \Slim\Router
	 * @autowire
	 */
	protected $router;

	/**
	 * @param string $menu
	 * @return string
	 */
	public function render($menu) {
		$result = '';

		if (!isset($this->config->get('menus')[$menu])) {
			return $result;
		}

		foreach ($this->config->get('menus')[$menu] as $item) {
			$result .= $this->renderItem($item);
		}

		return $result;
	}

	/**
	 * @param array $item
	 * @return string
	 */
	public function renderItem($item) {
		$url = '';

		if (isset($item['url'])) {
			$url = $item['url'];
		}

		if (empty($url) and isset($item['route'])) {
			$url = $this->router->pathFor($item['route']);
		}

		$label = isset($item['label']) ? $item['label'] : '';
		$items = isset($item['items']) ? $item['items'] : [];
		$link = !empty($label) ? '<a href="' . $url . '" class="item">' . $label . '</a>' : '';

		return $link . $this->renderSubItems($items);
	}

	/**
	 * @param array $items
	 * @return string
	 */
	public function renderSubItems($items) {
		if (empty($items)) {
			return;
		}

		return;
	}

}
