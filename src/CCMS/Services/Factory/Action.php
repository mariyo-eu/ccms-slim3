<?php

namespace CCMS\Services\Factory;

use CCMS\Services\Factory;
use CCMS\Interfaces;
use ReflectionException;

/**
 * Actions Factory.
 */
class Action extends Factory implements Interfaces\Service\Factory\Action {

    /**
     * @param string $class
     * @param array $params
     * @return Interfaces\Action
     * @throws ReflectionException
     */
	public function create($class, $params = array()) {
		return parent::create($class, $params);
	}

}
