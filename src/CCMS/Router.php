<?php

namespace CCMS;

use Slim;
use CCMS\Interfaces;

class Router extends Slim\Router implements Interfaces\Router {

	/**
	 * Build the path for a named route excluding the base path
	 *
	 * @param string $name        Route name
	 * @param array  $data        Named argument replacement data
	 * @param array  $queryParams Optional query string parameters
	 *
	 * @return string
	 *
	 * @throws RuntimeException         If named route does not exist
	 * @throws InvalidArgumentException If required data not provided
	 */
	public function relativePathFor($name, array $data = [], array $queryParams = []) {
		$route = $this->getNamedRoute($name);
		$pattern = $route->getPattern();

		$routeDatas = $this->routeParser->parse($pattern);
		// $routeDatas is an array of all possible routes that can be made. There is
		// one routedata for each optional parameter plus one for no optional parameters.
		//
        // The most specific is last, so we look for that first.
		$routeDatas = array_reverse($routeDatas);

		$segments = [];
		foreach ($routeDatas as $routeData) {
			foreach ($routeData as $item) {
				if (is_string($item)) {
					// this segment is a static string
					$segments[] = $item;
					continue;
				}

				// Does $item[0] contains "-"? Maybe its object argument 
				if (strpos($item[0], '-')) {
					$matches = array_filter(explode('-', $item[0]));
					$objectKey = array_shift($matches);

					// Does argument exists and is object?
					if (!empty($data[$objectKey]) and is_object($data[$objectKey])) {
						$object = $data[$objectKey];
						foreach ($matches as $match) {
							$object = $object->{$match};
						}
					}
				}

				if (isset($object)) {
					$segments[] = $object;
					unset($object);
				} else {

					// This segment has a parameter: first element is the name
					if (!array_key_exists($item[0], $data)) {
						// we don't have a data element for this segment: cancel
						// testing this routeData item, so that we can try a less
						// specific routeData item.
						$segments = [];
						$segmentName = $item[0];
						break;
					}
					$segments[] = $data[$item[0]];
				}
			}
			if (!empty($segments)) {
				// we found all the parameters for this route data, no need to check
				// less specific ones
				break;
			}
		}

		if (empty($segments)) {
			throw new InvalidArgumentException('Missing data for URL segment: ' . $segmentName);
		}
		$url = implode('', $segments);

		if ($queryParams) {
			$url .= '?' . http_build_query($queryParams);
		}

		return $url;
	}

}
