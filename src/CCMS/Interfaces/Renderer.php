<?php

namespace CCMS\Interfaces;

use Psr\Http\Message\ResponseInterface;

Interface Renderer {

	/**
	 * Renderer constructor.
	 *
	 * @param string $templatePath
	 * @param array $attributes
	 */
	public function __construct($templatePath = "", $attributes = []);

	/**
	 * Render a template
	 *
	 * $data cannot contain template as a key
	 *
	 * throws RuntimeException if $templatePath . $template does not exist
	 *
	 * @param ResponseInterface $response
	 * @param string             $template
	 * @param array              $data
	 *
	 * @return ResponseInterface
	 *
	 * @throws \InvalidArgumentException
	 * @throws \RuntimeException
	 */
	public function render(ResponseInterface $response, $template, array $data = []);

	/**
	 * Get the attributes for the renderer
	 *
	 * @return array
	 */
	public function getAttributes();

	/**
	 * Set the attributes for the renderer
	 *
	 * @param array $attributes
	 */
	public function setAttributes(array $attributes);

	/**
	 * Add an attribute
	 *
	 * @param $key
	 * @param $value
	 */
	public function addAttribute($key, $value);

	/**
	 * Retrieve an attribute
	 *
	 * @param $key
	 * @return mixed
	 */
	public function getAttribute($key);

	/**
	 * Get the template path
	 *
	 * @return string
	 */
	public function getTemplatePath();

	/**
	 * Set the template path
	 *
	 * @param string $templatePath
	 */
	public function setTemplatePath($templatePath);

	/**
	 * Renders a template and returns the result as a string
	 *
	 * cannot contain template as a key
	 *
	 * throws RuntimeException if $templatePath . $template does not exist
	 *
	 * @param $template
	 * @param array $data
	 *
	 * @return mixed
	 *
	 * @throws \InvalidArgumentException
	 * @throws \RuntimeException
	 */
	public function fetch($template, array $data = []);
}
