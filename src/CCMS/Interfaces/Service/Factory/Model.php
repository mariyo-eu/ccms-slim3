<?php

namespace CCMS\Interfaces\Service\Factory;

use CCMS\Interfaces\Service\Factory;
use CCMS\Interfaces;

Interface Model extends Factory {

	/**
	 * @param string $class
	 * @param mixed $row
	 * @return Interfaces\Model
	 */
	public function create($class, $row = NULL);
	
}
