<?php

namespace CCMS;

use CCMS\Interfaces;
use Interop\Container\Exception\ContainerException;
use ReflectionException;
use Slim\Interfaces\RouterInterface;
use Slim\Interfaces\Http\EnvironmentInterface;
use Slim\Interfaces\CallableResolverInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ReflectionClass;
use ReflectionParameter;
use ReflectionProperty;

class Container extends \Slim\Container implements Interfaces\Container
{
    /**
     * @var array
     */
    protected $classes;

    /**
     * @param string $class
     * @param array  $params
     *
     * @return object
     * @throws ContainerException
     * @throws ReflectionException
     */
    public function create(string $class, array $params = [])
    {
        $instance = $this->createInstance($class, $params);

        if ($instance instanceof Interfaces\Model\Item) {
            $this->prepareModelItem($instance);
        }

        return $instance;
    }

    /**
     * @param Interfaces\Model\Item $item
     *
     * @return mixed
     * @throws ReflectionException
     */
    protected function prepareModelItem(Interfaces\Model\Item $item)
    {
        // Load reflection
        $reflection = new ReflectionClass($item);
        $class      = $reflection->getName();

        // Init submodels
        if (!isset($this->classes[$class]['submodels'])) {
            $this->classes[$class]['submodels'] = [];
        }

        // Is class a Model Item, unset it to be able __get properties?
        if ($reflection->implementsInterface(Interfaces\Model\Item::class)) {
            $properties = $reflection->getProperties();

            foreach ($reflection->getTraits() as $trait) {
                $properties += $trait->getProperties();
            }

            foreach ($properties as $property) {
                /* @var $property ReflectionProperty */
                if (!$property->isPublic()) {
                    continue;
                }

                if (isset($model->{$property->name})) {
                    continue;
                }

                // Load @var annotation
                $varAnnotation = [];
                preg_match('~@var\s+([\w\\\]+)~sm', $property->getDocComment(), $varAnnotation);

                if (empty($varAnnotation[1])) {
                    continue;
                }

                // Prepare class name from @var annotation
                $propertyClass = trim((strpos($varAnnotation[1], '\\') === 0 ? '' : $reflection->getNamespaceName()) . '\\' . $varAnnotation[1], '\\');

                if (!class_exists($propertyClass)) {
                    continue;
                }

                // Load property reflection
                $propertyClassReflection = new \ReflectionClass($propertyClass);

                // Is property really property
                if ($propertyClassReflection->implementsInterface(Interfaces\Model\Item\Property::class)) {
                    $this->classes[$class]['submodels'][] = $property;
                }
            }
        }

        // Unset submodel property
        foreach ($this->classes[$class]['submodels'] as $submodel) {
            /* @var $submodel ReflectionProperty */
            unset($item->{$submodel->getName()});
        }
    }

    /**
     * @param string $class
     * @param array  $params
     *
     * @return object
     * @throws ContainerException
     * @throws ReflectionException
     */
    protected function createInstance(string $class, array $params = [])
    {
        // Create instance from cache
        if (isset($this->classes[$class])) {
            /** @var ReflectionClass $reflection */
            $reflection = $this->classes[$class]['reflection'];

            // Create instance from cache
            $instance = $reflection->newInstanceArgs($this->classes[$class]['constructor']);

            // Autowire instance properties from cache
            foreach ($this->classes[$class]['properties'] as $property) {
                /** @var ReflectionProperty $propertyReflection */
                $propertyReflection = $property['reflection'];
                $propertyReflection->setAccessible(true);
                $propertyReflection->setValue($instance, $property['instance']);
            }

            // Return instance
            return $instance;
        }

        $reflection = new ReflectionClass($class);

        // Init property reflections cache
        $this->classes[$class] = [
            'reflection'  => $reflection,
            'constructor' => [],
            'properties'  => [],
        ];

        // Create instance
        $instance = $reflection->getConstructor() ? $this->getInstancebyConstructor($reflection, $params) : new $class;

        // Autowire properties
        $this->autowireProperties($instance, $reflection);

        return $instance;
    }

    /**
     * @param ReflectionClass $reflection
     * @param array           $params
     *
     * @return object
     * @throws ContainerException
     * @throws ReflectionException
     */
    protected function getInstancebyConstructor(ReflectionClass $reflection, $params)
    {
        /* @var $reflectionParameters ReflectionParameter[] */
        $reflectionParameters = $reflection->getConstructor()->getParameters();

        // Skip provided params
        foreach ($params as $param) {
            if (!empty($reflectionParameters)) {
                array_shift($reflectionParameters);
            }
        }

        foreach ($reflectionParameters as $parameter) {
            $parameterClass = $parameter->getClass();

            if (empty($parameterClass)) {
                if ($parameter->isPassedByReference()) {
                    $null     = null;
                    $params[] = &$null;
                    continue;
                }

                $params[] = null;
                continue;
            }

            if ($this->has($parameterClass->getName())) {
                $params[] = $this->get($parameterClass->getName());
                continue;
            }

            $parameterInterfaces = $parameterClass->getInterfaces();
            if (empty($parameterInterfaces) and $parameterClass->isInterface()) {
                $parameterInterfaces[] = $parameterClass;
            }

            foreach ($parameterInterfaces as $parameterInterface) {
                $foundParameter = $this->findParameter($parameterInterface);
                if ($foundParameter !== false) {
                    $params[] = $foundParameter;
                    continue 2;
                }
            }

            if ($parameterClass->isInstantiable()) {
                $params[] = $this->create($parameterClass->getName());
                continue;
            }

            $params[] = null;
        }

        // Define constructor parameters cache
        $this->classes[$reflection->getName()]['constructor'] = $params;

        // Test if params is actually empty
        $filteredParams = array_filter($params);

        // Create new instance of class
        return $reflection->newInstanceArgs(empty($filteredParams) ? $filteredParams : $params);
    }

    /**
     * Find parameter according to Reflection
     *
     * @param ReflectionClass $parameterInterface
     *
     * @return bool|mixed|object
     * @throws ContainerException
     */
    protected function findParameter(ReflectionClass $parameterInterface)
    {
        if ($parameterInterface->implementsInterface(EnvironmentInterface::class)) {
            return $this->get('environment');
        }

        if ($parameterInterface->implementsInterface(RouterInterface::class)) {
            return $this->get('router');
        }

        if ($parameterInterface->implementsInterface(ServerRequestInterface::class)) {
            return $this->get('request');
        }

        if ($parameterInterface->implementsInterface(ResponseInterface::class)) {
            return $this->get('response');
        }

        if ($parameterInterface->implementsInterface(CallableResolverInterface::class)) {
            return $this->get('callableResolver');
        }

        return false;
    }

    /**
     * Autowire properties.
     *
     * @param mixed           $instance
     * @param ReflectionClass $reflection
     *
     * @throws ContainerException
     * @throws ReflectionException
     */
    protected function autowireProperties($instance, ReflectionClass $reflection)
    {
        /* @var $reflectionProperties ReflectionProperty[] */
        $reflectionProperties = $reflection->getProperties();

        foreach ($reflectionProperties as $reflectionProperty) {
            // Reset property instance
            $propertyInstance = null;

            // Load @autowire annotation
            $autowireAnnotation = [];
            preg_match('~@autowire\s+~sm', $reflectionProperty->getDocComment(), $autowireAnnotation);
            if (empty($autowireAnnotation)) {
                continue;
            }

            // Load @var annotation
            $varAnnotation = [];
            preg_match('~@var\s+([\w\\\]+)~sm', $reflectionProperty->getDocComment(), $varAnnotation);
            if (empty($varAnnotation[1])) {
                continue;
            }

            // Find reflection of property class from @var annotation
            $reflectionPropertyReflection = new ReflectionClass($varAnnotation[1]);

            // Try to find property in DI by class name
            if ($this->has($reflectionPropertyReflection->getName())) {
                $propertyInstance = $this->get($reflectionPropertyReflection->getName());
            }

            // Search for property instance
            if (empty($propertyInstance)) {
                $propertyInstance = $this->findParameter($reflectionPropertyReflection);
            }

            // Property instance, try to create
            if (empty($propertyInstance) && $reflectionPropertyReflection->isInstantiable()) {
                $propertyInstance = $this->create($reflectionPropertyReflection->getName());
            }

            // Property instance not found
            if (empty($propertyInstance)) {
                continue;
            }

            // Set property instance
            $reflectionProperty->setAccessible(true);
            $reflectionProperty->setValue($instance, $propertyInstance);

            // Init property reflections cache
            $this->classes[get_class($instance)]['properties'][] = [
                'instance'   => $propertyInstance,
                'reflection' => $reflectionProperty,
            ];
        }
    }
}
