<?php

namespace CCMS\Helpers;

abstract class Html {

	/**
	 * @var  array  preferred order of attributes
	 */
	public static $attributeOrder = array
		(
		'action',
		'method',
		'type',
		'id',
		'name',
		'value',
		'href',
		'src',
		'width',
		'height',
		'cols',
		'rows',
		'size',
		'maxlength',
		'rel',
		'media',
		'accept-charset',
		'accept',
		'tabindex',
		'accesskey',
		'alt',
		'title',
		'class',
		'style',
		'selected',
		'checked',
		'readonly',
		'disabled',
	);

	/**
	 * @var  string
	 */
	public static $charset = 'utf-8';

	/**
	 * @var  boolean  use strict XHTML mode?
	 */
	public static $strict = TRUE;

	/**
	 * @var  boolean  automatically target external URLs to a new window?
	 */
	public static $windowedUrls = FALSE;

	/**
	 * Convert special characters to HTML entities. All untrusted content
	 * should be passed through this method to prevent XSS injections.
	 *
	 *     echo Html::chars($username);
	 *
	 * @param   string  $value          string to convert
	 * @param   boolean $doubleEncode  encode existing entities
	 * @return  string
	 */
	public static function chars($value, $doubleEncode = TRUE) {
		return htmlspecialchars((string) $value, ENT_QUOTES, static::$charset, $doubleEncode);
	}

	/**
	 * Convert all applicable characters to HTML entities. All characters
	 * that cannot be represented in HTML with the current character set
	 * will be converted to entities.
	 *
	 *     echo Html::entities($username);
	 *
	 * @param   string  $value          string to convert
	 * @param   boolean $doubleEncode  encode existing entities
	 * @return  string
	 */
	public static function entities($value, $doubleEncode = TRUE) {
		return htmlentities((string) $value, ENT_QUOTES, static::$charset, $doubleEncode);
	}

	/**
	 * Create HTML link anchors. Note that the title is not escaped, to allow
	 * HTML elements within links (images, etc).
	 *
	 *     echo Html::anchor('/user/profile', 'My Profile');
	 *
	 * @param   string  $uri        URL or URI string
	 * @param   string  $title      link text
	 * @param   array   $attributes HTML anchor attributes
	 * @return  string
	 * @uses    Html::attributes
	 */
	public static function anchor($uri, $title = NULL, array $attributes = NULL) {
		if ($title === NULL) {
			// Use the URI as the title
			$title = $uri;
		}
		if ($uri === '') {
			// Only use the base URL
			$uri = '/';
		} else {
			if (strpos($uri, '://') !== FALSE) {
				if (static::$windowedUrls === TRUE AND empty($attributes['target'])) {
					// Make the link open in a new window
					$attributes['target'] = '_blank';
				}
			}
		}
		// Add the sanitized link to the attributes
		$attributes['href'] = $uri;
		return '<a' . static::attributes($attributes) . '>' . $title . '</a>';
	}

	/**
	 * Creates an HTML anchor to a file. Note that the title is not escaped,
	 * to allow HTML elements within links (images, etc).
	 *
	 *     echo Html::fileAnchor('media/doc/user_guide.pdf', 'User Guide');
	 *
	 * @param   string  $file       name of file to link to
	 * @param   string  $title      link text
	 * @param   array   $attributes HTML anchor attributes
	 * @return  string
	 * @uses    Html::attributes
	 */
	public static function fileAnchor($file, $title = NULL, array $attributes = NULL) {
		if ($title === NULL) {
			// Use the file name as the title
			$title = basename($file);
		}
		// Add the file link to the attributes
		$attributes['href'] = $file;
		return '<a' . static::attributes($attributes) . '>' . $title . '</a>';
	}

	/**
	 * Creates an email (mailto:) anchor. Note that the title is not escaped,
	 * to allow HTML elements within links (images, etc).
	 *
	 *     echo Html::mailto($address);
	 *
	 * @param   string  $email      email address to send to
	 * @param   string  $title      link text
	 * @param   array   $attributes HTML anchor attributes
	 * @return  string
	 * @uses    Html::attributes
	 */
	public static function mailto($email, $title = NULL, array $attributes = NULL) {
		if ($title === NULL) {
			// Use the email address as the title
			$title = $email;
		}
		return '<a href="&#109;&#097;&#105;&#108;&#116;&#111;&#058;' . $email . '"' . static::attributes($attributes) . '>' . $title . '</a>';
	}

	/**
	 * Creates a style sheet link element.
	 *
	 *     echo Html::style('media/css/screen.css');
	 *
	 * @param   string  $file       file name
	 * @param   array   $attributes default attributes
	 * @return  string
	 * @uses    Html::attributes
	 */
	public static function style($file, array $attributes = NULL) {
		// Set the stylesheet link
		$attributes['href'] = $file;
		// Set the stylesheet rel
		$attributes['rel'] = empty($attributes['rel']) ? 'stylesheet' : $attributes['rel'];
		// Set the stylesheet type
		$attributes['type'] = 'text/css';
		return '<link' . static::attributes($attributes) . ' />';
	}

	/**
	 * Creates a script link.
	 *
	 *     echo Html::script('media/js/jquery.min.js');
	 *
	 * @param   string  $file       file name
	 * @param   array   $attributes default attributes
	 * @return  string
	 * @uses    Html::attributes
	 */
	public static function script($file, array $attributes = NULL) {
		// Set the script link
		$attributes['src'] = $file;
		// Set the script type
		$attributes['type'] = 'text/javascript';
		return '<script' . static::attributes($attributes) . '></script>';
	}

	/**
	 * Creates a image link.
	 *
	 *     echo Html::image('media/img/logo.png', array('alt' => 'My Company'));
	 *
	 * @param   string  $file       file name
	 * @param   array   $attributes default attributes
	 * @return  string
	 * @uses    Html::attributes
	 */
	public static function image($file, array $attributes = NULL) {
		// Add the image link
		$attributes['src'] = $file;
		return '<img' . static::attributes($attributes) . ' />';
	}

	/**
	 * Compiles an array of HTML attributes into an attribute string.
	 * Attributes will be sorted using static::$attributeOrder for consistency.
	 *
	 *     echo '<div'.Html::attributes($attrs).'>'.$content.'</div>';
	 *
	 * @param   array   $attributes attribute list
	 * @return  string
	 */
	public static function attributes(array $attributes = NULL) {
		if (empty($attributes))
			return '';
		
		$sorted = array();
		foreach (static::$attributeOrder as $key) {
			if (isset($attributes[$key])) {
				// Add the attribute to the sorted list
				$sorted[$key] = $attributes[$key];
			}
		}
		
		// Combine the sorted attributes
		$attributes = $sorted + $attributes;
		$compiled = '';
		foreach ($attributes as $key => $value) {
			if ($value === NULL) {
				// Skip attributes that have NULL values
				continue;
			}
			
			if (is_int($key)) {
				// Assume non-associative keys are mirrored attributes
				$key = $value;
				if (!static::$strict) {
					// Just use a key
					$value = FALSE;
				}
			}
			
			// Add the attribute key
			$compiled .= ' ' . $key;
			if ($value OR static::$strict) {
				// Add the attribute value
				$compiled .= '="' . static::chars($value) . '"';
			}
		}
		
		return $compiled;
	}

}
