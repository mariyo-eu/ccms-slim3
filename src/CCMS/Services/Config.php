<?php

namespace CCMS\Services;

use CCMS\Interfaces;

/**
 * Service provides configuration from config files.
 */
class Config extends Base implements Interfaces\Service\Config {

	/**
	 * @var array
	 */
	protected $config = [];

	/**
	 * @var \CCMS\Interfaces\Container
	 * @autowire
	 */
	protected $container;

	/**
	 * @param string $configName
	 * @return array
	 */
	public function get($configName = FALSE) {
		if ($configName === FALSE) {
			return $this->config;
		}

		if (!isset($this->config[$configName])) {
			$this->config[$configName] = \CCMS\registerConfig($configName . '.php', $this->container);
		}

		return $this->config[$configName];
	}

}
