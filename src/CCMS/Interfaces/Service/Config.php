<?php

namespace CCMS\Interfaces\Service;

use CCMS\Interfaces\Service;

/**
 * Service provides configuration from config files.
 */
Interface Config extends Service {

	/**
	 * @param string|FALSE $configName
	 * @return array|NULL
	 */
	public function get($configName = FALSE);
}
