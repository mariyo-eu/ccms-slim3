<?php

namespace CCMS\Interfaces\Service\Event;

use CCMS\Interfaces\Service;
use Zend\EventManager\EventManagerInterface;

Interface Manager extends Service, EventManagerInterface {
	
}
