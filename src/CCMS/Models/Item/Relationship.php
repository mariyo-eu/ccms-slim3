<?php

namespace CCMS\Models\Item;

use CCMS\Models\Item;
use CCMS\Interfaces;

abstract class Relationship extends Item implements Interfaces\Model\Item\Relationship {

	/**
	 * @var string
	 */
	protected $item = Item::class;

	/**
	 * @var string
	 */
	public function getItemType() {
		return $this->item;
	}

	/**
	 * Set row.
	 * @param mixed $rows
	 * @return $this
	 */
	public function set($rows) {
		if (empty($rows)) {
			return $this;
		}

		// Reset values
		$this->data['value'] = [];

		foreach ($rows as $rowKey => $row) {
			// Load value from ID
			if (is_scalar($row)) {
				$this->data['value'][$row] = $this->repository->find($row);
				continue;
			}

			// Load value from ID
			if ($row instanceof Item) {
				$this->data['value'][$rowKey] = $row;
				continue;
			}

			// Skip broken data
			if (!empty($row['related_id'])) {
				$value = $this
						->model
						->create($this->getItemType())
						->set($row)
						->set(array('id' => $row['related_id'], 'type' => $this->getItemType()));

				if (!empty($value)) {
					$this->data['value'][$row['related_id']] = $value;
				}

				continue;
			}

			if (empty($row['value'])) {
				continue;
			}

			$value = $this
					->repository
					->find($row['value']);

			if (!empty($value)) {
				$this->data['value'][$row['value']] = $value;
			}
		}

		return $this;
	}

	/**
	 * Get value.
	 * @return mixed
	 */
	public function value() {
		if (isset($this->data['value'])) {
			return $this->data['value'];
		}

		if (isset($this->data['related_id'])) {
			return $this
							->repository
							->findRelated($this->data['related_id'], $this)
							->value();
		}

		return NULL;
	}

}
