<?php

namespace CCMS\Models;

use CCMS\Interfaces;
use CCMS\Traits;
use Traversable;
use ReflectionClass;

abstract class Item implements Interfaces\Model {

	use Traits\Translatable;

	/**
	 * @var array
	 */
	protected $data = [];

	/**
	 * @var array
	 */
	protected $properties = [];

	/**
	 * @var \CCMS\Interfaces\Container
	 * @autowire
	 */
	protected $ci;

	/**
	 * @var \CCMS\Interfaces\Service\Factory\Model
	 * @autowire
	 */
	protected $model;

	/**
	 * @var \CCMS\Interfaces\Service\Repository
	 * @autowire
	 */
	public $repository;

	/**
	 * Catching inside property call
	 * @param type $name
	 * @return bool
	 */
	public function hasProperty($name) {
		// Property is already set
		if (isset($this->properties[$name])) {
			return TRUE;
		}

		// Load reflection
		$reflection = new ReflectionClass($this);

		// Check if there is such property defined
		if (!$reflection->hasProperty($name)) {
			return FALSE;
		}

		// Load @var annotation
		$varAnnotation = array();
		preg_match('~@var\s+([\w\\\]+)~sm', $reflection->getProperty($name)->getDocComment(), $varAnnotation);

		if (empty($varAnnotation[1])) {
			return isset($this->properties[$name]) ? $this->properties[$name] : $this->{$name};
		}

		// Set this reflection as default for checking correct property class name fro mannotations
		$reflectionForProperty = $reflection;

		do {
			// Prepare class name from @var annotation
			$propertyClass = (strpos($varAnnotation[1], '\\') === 0 ? '' : $reflectionForProperty->getNamespaceName()) . '\\' . $varAnnotation[1];

			if (!class_exists($propertyClass) and $reflectionForProperty->getParentClass()) {
				$reflectionForProperty = new ReflectionClass($reflectionForProperty->getParentClass()->getName());
				continue;
			}

			// Fallback
			if (!class_exists($propertyClass)) {
				return $this->properties[$name];
			}
		} while (!class_exists($propertyClass));

		// Load property reflection
		$propertyClassReflection = new ReflectionClass($propertyClass);

		// Is property really property
		if (!$propertyClassReflection->implementsInterface(Interfaces\Model\Item\Property::class)) {
			// In case of extendict Items, this is sometimes not provided
			if (!isset($this->properties[$name])) {
				return FALSE;
			}

			return $this->properties[$name];
		}

		// Create new property instance
		$this->properties[$name] = $this
				->model
				->create($propertyClassReflection->getName());

		return TRUE;
	}

	/**
	 * Catching inside property call
	 * @param type $name
	 * @return type
	 */
	public function property($name) {
		// Property is already set
		if (isset($this->properties[$name])) {
			return $this->properties[$name];
		}

		// Check if there is such property defined
		if (!$this->hasProperty($name)) {
			return NULL;
		}

		// Create new property instance
		if ($this instanceof Interfaces\Model\Item) {
			return $this->properties[$name] = $this
					->repository
					->findByParent($this->pk(), get_class($this->properties[$name]));
		}

		// Create new property instance
		return $this->properties[$name] = $this
				->repository
				->findByParent($this->getData('related_id'), $this->getData('relation'));
	}

	/**
	 * Catching outside properties call.
	 * @param type $name
	 * @return type
	 */
	public function __get($name) {
		return $this->property($name);
	}

	/**
	 * Catching outside properties call.
	 * @param type $name
	 * @return type
	 */
	public function __set($name, $value) {
		if (
				$this->hasProperty($name)
				and $this->properties[$name] instanceof Interfaces\Model\Item\Property
				and ! $this->properties[$name] instanceof Interfaces\Model\Item
		) {
			return $this
							->properties[$name]
							->setValue($value);
		}
	}

	/**
	 * @return string
	 */
	public function __toString() {
		return (string) $this->value();
	}

	/**
	 * Set row - it should be used when trying to set values to properties.
	 * If there is need to update data, use setData(NULL, $row).
	 * 
	 * @param mixed $row
	 * @return $this
	 */
	public function set($row) {
		if (is_scalar($row) or empty($row)) {
			return $this->setValue($row);
		}

		foreach ($row as $column => $value) {
			if (!$this->hasProperty($column)) {
				$this->setData($column, $value);
				continue;
			}

			$this
					->properties[$column]
					->set($value);
		}

		return $this;
	}

	/**
	 * Set value.
	 * @param mixed $value
	 * @return $this
	 */
	public function setValue($value) {
		return $this->setData('value', $value);
	}

	/**
	 * Get value.
	 * @return mixed
	 */
	public function getValue() {
		return $this->value();
	}

	/**
	 * Shortcut for Get value.
	 * @return mixed
	 */
	public function value() {
		return isset($this->data['value']) ? $this->data['value'] : NULL;
	}

	/**
	 * Set data - do not use when try to set values of properties.
	 * It set internal data of each model Item.
	 * If there is need to update values of properties, use method set($value).
	 * 
	 * @param string|array $key
	 * @param mixed $value
	 * @return $this
	 */
	public function setData($key, $value) {
		if ($value instanceof Traversable) {
			$value = iterator_to_array($value);
		}

		if ($key === NULL and is_array($value)) {
			$this->data = $value;
			return $this;
		}

		$this->data[$key] = $value;
		return $this;
	}

	/**
	 * Get value from data.
	 * @return mixed
	 */
	public function getData($key = NULL) {
		if (empty($key)) {
			return $this->data;
		}

		if (isset($this->data[$key])) {
			return $this->data[$key];
		}

		return NULL;
	}

	/**
	 * Get value from data.
	 * @return array
	 */
	public function getRouteData() {
		$data = $this->getData();
		$data['_'] = $this;
		return $data;
	}

	/**
	 * Get primary key value from data.
	 * @return int
	 */
	public function pk() {
		return $this->getData('id');
	}

	/**
	 * Model cleans after itself.
	 * @return \CCMS\Interfaces\Model
	 */
	public function cleanup() {
		return $this;
	}

	/**
	 * Model returns all property items.
	 * @return array
	 */
	public function getProperties() {
		return $this->properties;
	}

}
