<?php

namespace CCMS\Traits;

use Ohanzee\Helper\Arr;

trait Status {

	public function getValues() {
		return [
			'0' => $this->_t('Draft'),
			'1' => $this->_t('Published'),
			'-1' => $this->_t('Deleted')
		];
	}

	public function __toString() {
		return Arr::get($this->getValues(), $this->value(), '');
	}

}
