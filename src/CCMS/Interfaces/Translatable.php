<?php

namespace CCMS\Interfaces;

Interface Translatable {

	/**
	 * @param   string  $string		text to translate
	 * @param   array   $values		values to replace in the translated text
	 * @param   int		$count		number for plural form
	 * @return  string
	 */
	public function _t($string, array $values = NULL, $count = FALSE);
}
