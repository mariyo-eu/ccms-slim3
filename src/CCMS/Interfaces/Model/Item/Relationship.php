<?php

namespace CCMS\Interfaces\Model\Item;

Interface Relationship {

	/**
	 * @var string
	 */
	public function getItemType();

	/**
	 * Set row.
	 * @param mixed $rows
	 * @return $this
	 */
	public function set($rows);
}
