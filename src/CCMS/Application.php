<?php

namespace CCMS;

use CCMS\Interfaces\Container;
use CCMS\Interfaces\Service\Validator\Host;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\App;
use Slim\Interfaces\RouteInterface;

class Application extends App implements Interfaces\Application
{
    /**
     * @var Container
     */
    private $container;

    /**
     * @var Host
     */
    private $hostValidator;

    /**
     * @param Container $container
     */
    public function __construct(Container $container, Host $hostValidator)
    {
        parent::__construct($container);
        $this->container     = $container;
        $this->hostValidator = $hostValidator;
    }

    /**
     * @param array  $methods
     * @param string $pattern
     * @param string $actionKey
     *
     * @return RouteInterface
     */
    public function route(array $methods, string $pattern, string $actionKey): RouteInterface
    {
        $container     = $this->container;
        $hostValidator = $this->hostValidator;

        return $this
            ->map($methods, $pattern, function (RequestInterface $request, ResponseInterface $response, array $args) use ($container, $actionKey) {
                $action = $container->has($actionKey) ? $container->get($actionKey) : $container->create($actionKey);

                return $action($request, $response, $args);
            })
            ->setName($actionKey)
            ->add(function (RequestInterface $request, ResponseInterface $response, callable $next) use ($actionKey, $hostValidator) {
                // Host validator service
                if ($hostValidator->isValidHost($request, $actionKey) === false) {
                    $validUrl = $hostValidator->getValidUrl($request, $actionKey);

                    return $response->withHeader('Location: ' . $validUrl, 302);
                }

                return $next($request, $response);
            });
    }
}
