<?php

namespace CCMS\Helpers;

abstract class Strings
{

    public static function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }

    /**
     * '1.999,369€' -> float(1999.369)
     * '126,564,789.33 m²' -> 126564789.33
     *
     * @param string $num
     *
     * @return float
     */
    public static function toFloat($num)
    {
        $dotPos   = strrpos($num, '.');
        $commaPos = strrpos($num, ',');
        $sep      = (($dotPos > $commaPos) && $dotPos)
            ? $dotPos
            :
            ((($commaPos > $dotPos) && $commaPos) ? $commaPos : false);

        if (!$sep) {
            return floatval(preg_replace("/[^0-9]/", "", $num));
        }

        return floatval(
            preg_replace("/[^0-9]/", "", substr($num, 0, $sep)) . '.' .
            preg_replace("/[^0-9]/", "", substr($num, $sep + 1, strlen($num)))
        );
    }

    /**
     * Transform camelCase into camel Case
     *
     * @param string $string
     *
     * @return string
     */
    public static function deliciousCamelcase($string)
    {
        $regexp = '/
          (?<=[a-z])
          (?=[A-Z])
        | (?<=[A-Z])
          (?=[A-Z][a-z])
        /x';

        return implode(' ', preg_split($regexp, $string));
    }

    /**
     * Reverse function to "parse_url()".
     *
     * @param array $parsedUrl
     * @param array $ommit
     *
     * @return string
     */
    public static function unparseUrl($parsedUrl, $ommit = [])
    {
        $p             = [];
        $p['scheme']   = isset($parsedUrl['scheme']) ? $parsedUrl['scheme'] . '://' : '';
        $p['user']     = isset($parsedUrl['user']) ? $parsedUrl['user'] : '';
        $p['pass']     = isset($parsedUrl['pass']) ? ':' . $parsedUrl['pass'] : '';
        $p['pass']     = ($p['user'] || $p['pass']) ? $p['pass'] . "@" : '';
        $p['host']     = isset($parsedUrl['host']) ? $parsedUrl['host'] : '';
        $p['port']     = isset($parsedUrl['port']) ? ':' . $parsedUrl['port'] : '';
        $p['path']     = isset($parsedUrl['path']) ? $parsedUrl['path'] : '';
        $p['query']    = isset($parsedUrl['query']) ? '?' . $parsedUrl['query'] : '';
        $p['fragment'] = isset($parsedUrl['fragment']) ? '#' . $parsedUrl['fragment'] : '';

        if ($ommit) {
            foreach ($ommit as $key) {
                if (isset($p[$key])) {
                    $p[$key] = '';
                }
            }
        }

        return implode('', $p);

        // return $p['scheme'] . $p['user'] . $p['pass'] . $p['host'] . $p['port'] . $p['path'] . $p['query'] . $p['fragment'];
    }

    /**
     * @param string $class
     *
     * @return string
     */
    public static function classNameToPath(string $class): string
    {
        return str_replace('\\', '/', strtolower($class));
    }

}
