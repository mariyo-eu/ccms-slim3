<?php

namespace CCMS\Interfaces\Service;

use CCMS\Interfaces;

Interface Repository {

	/**
	 * @return string
	 */
	public function getTable();

	/**
	 * @return string
	 */
	public function getTableValues();

	/**
	 * @param int $id
	 * @return Interfaces\Model
	 */
	public function find($id);

	/**
	 * @param int $parent
	 * @param string $relation
	 * @return Interfaces\Model
	 */
	public function findByParent($parent, $relation);

	/**
	 * @param string $relation
	 * @param string $value
	 * @return Interfaces\Model
	 */
	public function findByProperty($relation, $value);

	/**
	 * @param string $relation
	 * @param string $value
	 * @return Interfaces\Model[]
	 */
	public function findAllByProperty($relation, $value);

	/**
	 * @param string $value
	 * @param string $type
	 * @return array
	 */
	public function findByValue($value, $type);

	/**
	 * @param string $type
	 * @return []
	 */
	public function findItemsByType($type);

	/**
	 * @param Interfaces\Model  $model
	 * @return array
	 */
	public function findProperties(Interfaces\Model $model);

	/**
	 * @param int $parent
	 * @param \CCMS\Interfaces\Model\Item\Relationship $model
	 * @return \CCMS\Interfaces\Model\Item\Relationship
	 */
	public function findRelated($parent, Interfaces\Model\Item\Relationship $model);

	/**
	 * @param string $relation
	 * @param string $relatedID
	 * @return \CCMS\Interfaces\Model\Item|NULL
	 */
	public function findRelationParent($relation, $relatedID);

	/**
	 * @param Interfaces\Model $model
	 * @return Interfaces\Model
	 */
	public function findRoot(Interfaces\Model $model);

	/**
	 * @param Interfaces\Model $model
	 */
	public function save(Interfaces\Model $model);

	/**
	 * @param Interfaces\Model $model
	 */
	public function remove(Interfaces\Model $model);

	/**
	 * @return boolean
	 */
	public function checkStructure();

	/**
	 * Return data of the Model and all its properties
	 * 
	 * @param Interfaces\Model $model
	 * @return array
	 */
	public function getAsArray(Interfaces\Model $model);
}
