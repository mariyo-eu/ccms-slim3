<?php

namespace CCMS\Services\Repository;

use CCMS\Interfaces;
use Exception;

/**
 * Basic Item repository.
 */
class Item implements Interfaces\Service\Repository {

    /**
     * @var string
     */
    protected $table = 'items';

    /**
     * @var string
     */
    protected $primary = 'id';

    /**
     * @var string
     */
    protected $foreign = 'parent';

    /**
     * @var \CCMS\Interfaces\Service\Factory\Model
     * @autowire
     */
    protected $model;

    /**
     * @var \NotORM
     * @autowire
     */
    protected $notOrm;

    /**
     * @var \PDO
     * @autowire
     */
    protected $pdo;

    /**
     * @var \CCMS\Interfaces\Service\Event\Manager
     * @autowire
     */
    protected $events;

    /**
     * @var string
     */
    protected $pathToSourceSqlStructure = '/vendor/mariyo-eu/ccms-slim3/sql/items_structure.sql';

    /**
     * @return string
     */
    public function getTable() {
        return $this->table;
    }

    /**
     * @return string
     */
    public function getTableValues() {
        return $this->getTable() . '_values';
    }

    /**
     * @return boolean
     */
    public function checkStructure() {
        try {
            foreach ([$this->getTable(), $this->getTableValues()] as $table) {
                if (!$this->pdo->query("SELECT 1 FROM $table LIMIT 1")) {
                    return $this->createStructureFrom(realpath(PROTECTED_DIR . $this->pathToSourceSqlStructure));
                }
            }
            return TRUE;
        } catch (Exception $e) {
            // We got an exception == table not found
            return $this->createStructureFrom(realpath(PROTECTED_DIR . $this->pathToSourceSqlStructure));
        }
    }

    /**
     * @param type $pathToSourceSqlStructure
     * @return boolean
     */
    protected function createStructureFrom($pathToSourceSqlStructure) {
        if (empty($pathToSourceSqlStructure)) {
            return FALSE;
        }

        $structure = str_replace('items', $this->getTable(), file_get_contents($pathToSourceSqlStructure));
        $this->pdo->query($structure);

        return TRUE;
    }

    /**
     * @return Interfaces\Model
     */
    public function find($id) {
        $row = $this
            ->notOrm
            ->{$this->getTable()}
            ->where('id = ?', $id)
            ->fetch();

        if (!class_exists($row['type'])) {
            return FALSE;
        }

        return $this
            ->model
            ->create($row['type'])
            ->setData('id', $row['id'])
            ->setData('type', $row['type']);
    }

    /**
     * @param int $parent
     * @param string $relation
     * @return Interfaces\Model
     */
    public function findByParent($parent, $relation) {
        $results = $this
            ->notOrm
            ->{$this->getTableValues()}
            ->where('relation = ?', $relation)
            ->where($this->getTable() . '_id = ?', $parent);

        $property = $this
            ->model
            ->create($relation);

        if (!$property instanceof Interfaces\Model\Item\Relationship) {
            $row = $results->fetch();
            return $property
                ->set($row)
                ->set(array('id' => $row['related_id'], 'type' => $relation));
        }

        /* @var $property Interfaces\Model\Item\Relationship */
        return $property->set($results);
    }

    /**
     * @param string $relation
     * @param string $value
     * @return Interfaces\Model
     */
    public function findByProperty($relation, $value) {
        $rows = $this
            ->notOrm
            ->{$this->getTableValues()}
            ->where('relation = ?', $relation)
            ->where('value = ?', $value);

        foreach ($rows as $row) {
            return $this->find($row[$this->getTable() . '_id']);
        }

        return FALSE;
    }

    /**
     * @param string $relation
     * @param string $value
     * @return Interfaces\Model[]
     */
    public function findAllByProperty($relation, $value) {
        $rows = $this
            ->notOrm
            ->{$this->getTableValues()}
            ->where('relation = ?', $relation)
            ->where('value = ?', $value);

        $results = [];
        foreach ($rows as $row) {
            $results[$row[$this->getTable() . '_id']] = $this->find($row[$this->getTable() . '_id']);
        }

        return $results;
    }

    /**
     * @param string $type
     * @return []
     */
    public function findItemsByType($type) {
        $results = $this
            ->notOrm
            ->{$this->getTable()}
            ->where('type = ?', $type);

        $items = [];
        foreach ($results as $result) {
            $item = $this->model->create($type, $result);
            $items[$item->pk()] = $item;
        }

        return $items;
    }

    /**
     * @param int $parent
     * @param \CCMS\Interfaces\Model\Item\Relationship $model
     * @return \CCMS\Interfaces\Model\Item\Relationship
     */
    public function findRelated($parent, Interfaces\Model\Item\Relationship $model) {
        $rows = $this
            ->notOrm
            ->{$this->getTableValues()}
            ->where('relation = ?', get_class($model))
            ->where($this->getTable() . '_id = ?', $parent);

        return $model->setValue($rows);
    }

    /**
     * @param string $relation
     * @param string $relatedID
     * @return \CCMS\Interfaces\Model\Item|NULL
     */
    public function findRelationParent($relation, $relatedID) {
        $row = $this
            ->notOrm
            ->{$this->getTableValues()}
            ->where('relation = ?', $relation)
            ->where('related_id = ?', $relatedID)
            ->limit(1)
            ->fetch();

        if (!empty($row[$this->getTable() . '_id'])) {
            return $this->find($row[$this->getTable() . '_id']);
        }

        return NULL;
    }

    /**
     * TODO
     * @param Model $model
     * @return Model
     */
    public function findRoot(Interfaces\Model $model) {
        while ($model->parent) {
            $parent = $this->find($model->parent);
            return $this->findRoot($parent);
        }
        return $model;
    }

    /**
     * @param string $value
     * @param string $type
     * @return array
     */
    public function findByValue($value, $type) {
        return array_map(
            'iterator_to_array', iterator_to_array(
                $this
                    ->notOrm
                    ->{$this->getTable()}
                    ->where('type = ?', $type)
                    ->where('value = ?', $value)
            )
        );
    }

    /**
     * @param Interfaces\Model $model
     * @return array
     */
    public function findProperties(Interfaces\Model $model) {
        $rows = $this
            ->notOrm
            ->{$this->getTableValues()}
            ->where($this->getTable() . '_id = ?', $model->pk());

        $results = [];
        foreach ($rows as $row) {
            /* @var $row \NotORM_Row */
            $property = $this
                ->model
                ->create($row['relation']);

            // Scalar property
            if (empty($row['related_id'])) {
                $results[] = $property->set($row);
                continue;
            }

            // Property is a 1:1 relationship with another Item
            if ($property instanceof Interfaces\Model\Item) {
                $results[] = $this->find($row['related_id']);
                continue;
            }

            if ($property instanceof Interfaces\Model\Item\Relationship) {
                $data = $row->getIterator()->getArrayCopy();

                // Unset value to allow find related when accessing value = lazy relationship loading
                if (array_key_exists('value', $data)) {
                    unset($data['value']);
                }

                $results[] = $property->setData(NULL, $data);
                continue;
            }
        }

        return $results;
    }

    /**
     * @param Model $model
     * @return Model
     */
    public function remove(Interfaces\Model $model) {
        foreach ($this->findProperties($model) as $property) {
            if ($property instanceof Interfaces\Model\Item) {
                $this->remove($property);
            }
        }

        // Cleanup after itself
        $model->cleanup();

        return $this
            ->notOrm
            ->{$this->getTable()}
            ->where('id = ?', $model->pk())
            ->where('type = ?', get_class($model))
            ->delete();
    }

    /**
     * @param Interfaces\Model $item
     * @return int
     */
    public function save(Interfaces\Model $item) {
        if ($item instanceof Interfaces\Model\Item) {
            return $this->saveItem($item);
        }

        if ($item instanceof Interfaces\Model\Item\Property) {
            return $this->saveProperty($item);
        }

        return FALSE;
    }

    /**
     * @param Interfaces\Model\Item $item
     * @return int
     */
    protected function saveItem(Interfaces\Model\Item $item) {
        // Create item
        if (!$item->pk()) {
            $item->setData(NULL, $this->insert($item));
            $isInsert = TRUE;
        }

        // If Item is Property also
        if ($item instanceof Interfaces\Model\Item\Property) {
            $item->setData('related_id', $item->pk());
            $this->saveProperty($item);
        }

        // Save sub-items
        foreach ($item->getProperties() as $property) {
            if (!($property instanceof Interfaces\Model\Item or $property instanceof Interfaces\Model\Item\Property)) {
                continue;
            }

            // Prepare Item
            if ($property instanceof Interfaces\Model\Item) {
                $property->setData('related_id', $item->pk());
            }

            // Prepare Property
            if ($property instanceof Interfaces\Model\Item\Property) {
                $property->setData($this->getTable() . '_id', $item->pk());
            }

            // Save Property
            $this->save($property);
        }

        $this
            ->events
            ->trigger('model.item.' . (isset($isInsert) ? 'insert' : 'update'), NULL, [&$item]);

        return $this;
    }

    /**
     * @param Interfaces\Model\Item\Property $property
     * @return int
     */
    protected function saveProperty(Interfaces\Model\Item\Property $property) {
        $escaped = $this->escape(Interfaces\Model\Item\Property::class, $property);

        // Delete if exists
        $this
            ->notOrm
            ->{$this->getTableValues()}
            ->where($this->getTable() . '_id = ?', $escaped[$this->getTable() . '_id'])
            ->where('relation = ?', $escaped['relation'])
            ->delete();

        if ($property instanceof Interfaces\Model\Item\Relationship) {
            $relatedProperties = $property->value();

            if (!empty($relatedProperties)) {
                foreach ($relatedProperties as $relatedProperty) {
                    $this
                        ->notOrm
                        ->{$this->getTableValues()}
                        ->insert(
                            [
                                $this->getTable() . '_id' => $property->getData($this->getTable() . '_id'),
                                'related_id' => $relatedProperty->pk(),
                                'relation' => get_class($property),
                                'value' => $relatedProperty->value(),
                            ]
                        );
                }
            }

            return TRUE;
        }

        return $this
            ->notOrm
            ->{$this->getTableValues()}
            ->insert($escaped);
    }

    /**
     * @param Interfaces\Model\Item $item
     * @return int
     */
    protected function insert(Interfaces\Model\Item $item) {
        return $this
            ->notOrm
            ->{$this->getTable()}
            ->insert($this->escape(Interfaces\Model\Item::class, $item, array('type' => get_class($item))));
    }

    /**
     * @param Interfaces\Model $item
     * @return array
     */
    protected function escape($interface, Interfaces\Model $item, $extra = NULL) {
        $data = [];

        if ($interface == Interfaces\Model\Item::class) {
            $data = [
                'id' => NULL,
                'type' => get_class($item)
            ];
        }

        if ($interface == Interfaces\Model\Item\Property::class) {
            $data = [
                $this->getTable() . '_id' => NULL,
                'related_id' => NULL,
                'relation' => get_class($item),
                'lang' => NULL,
                'value' => NULL
            ];
        }

        $itemData = $item->getData();
        foreach (array_keys($data) as $dataKey) {
            if (isset($itemData[$dataKey])) {
                $data[$dataKey] = $itemData[$dataKey];
            }
        }

        if (!empty($extra)) {
            foreach ($extra as $extraKey => $extraValue) {
                $data[$extraKey] = $extraValue;
            }
        }

        return $data;
    }

    /**
     * Return data of the Model and all its properties
     *
     * @param Interfaces\Model $model
     * @return array
     */
    public function getAsArray(Interfaces\Model $model) {
        $results = [];

        foreach (array_keys(get_class_vars(get_class($model))) as $propertyName) {
            $property = $model->{$propertyName};

            if ($property instanceof Interfaces\Model\Item\Relationship) {
                $results[$propertyName] = [];
                foreach ($property->value() as $relatedItem) {
                    $results[$propertyName][] = $this->getAsArray($relatedItem);
                }
                continue;
            }

            if ($property instanceof Interfaces\Model\Item\Property) {
                $results[$propertyName] = $property->value();
                continue;
            }
        }

        return $results;
    }

}
