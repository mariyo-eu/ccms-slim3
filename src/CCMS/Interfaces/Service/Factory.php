<?php

namespace CCMS\Interfaces\Service;

interface Factory
{

    /**
     * @param string $class
     *
     * @deprecated
     */
    public function create($class);
}
