<?php

namespace CCMS\Services\Factory;

use CCMS\Services\Factory;
use CCMS\Interfaces;
use ReflectionException;

/**
 * Repository Factory.
 */
class Repository extends Factory implements Interfaces\Service\Factory\Repository {

    /**
     * @param string $class
     * @param array $params
     * @return object
     * @throws ReflectionException
     */
    public function create($class, $params = array()) {
		/* @var $repository Interfaces\Service\Factory\Repository */
		$repository = parent::create($class, $params);
		$repository->checkStructure();
		return $repository;
	}

}
