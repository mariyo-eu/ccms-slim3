<?php

namespace CCMS\Interfaces\Service\Factory;

use CCMS\Interfaces\Service\Factory;
use CCMS\Interfaces;

Interface Repository extends Factory {
	

	/**
	 * @param string $class
	 * @return Interfaces\Repository
	 */
	public function create($class);
	
}
