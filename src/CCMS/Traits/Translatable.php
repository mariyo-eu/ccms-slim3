<?php

namespace CCMS\Traits;

trait Translatable {

	/**
	 * @var \CCMS\Interfaces\Service\Translator
	 * @autowire
	 */
	protected $_translator;

	/**
	 * @param   string  $string		text to translate
	 * @param   array   $values		values to replace in the translated text
	 * @param   int		$count		number for plural form
	 * @return  string
	 */
	public function _t($string, array $values = NULL, $count = FALSE) {
		// Context
		$context = str_replace('\\', '/', strtolower(get_called_class()));

		// Translate
		return $this->_translator->_t($string, $context, $values, $count);
	}

}
