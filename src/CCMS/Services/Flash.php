<?php

namespace CCMS\Services;

use CCMS\Interfaces;
use Slim\Flash\Messages;

/**
 * Service Flash Messages.
 */
class Flash extends Messages implements Interfaces\Service\Flash {
	
}
