<?php

namespace CCMS\Interfaces\Model\Item;

Interface Search {

	/**
	 * Transform itself to Search Result
	 * @return Search\Result
	 */
	public function toSearchResult();
}
