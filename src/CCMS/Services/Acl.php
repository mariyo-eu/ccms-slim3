<?php

namespace CCMS\Services;

use Slim\Route;
use Slim\Http\Response;
use Zend\Authentication;
use CCMS\Interfaces\Service;
use CCMS\Models\User;
use Exception;

/**
 * Service provides ACL.
 */
class Acl extends Base implements Service\Acl {

	/**
	 * Name for default Route. Route with this name will be allowed by ACL automatically.
	 */
	const DEFAULT_ROUTE = 'default_route_name';

	/**
	 * @var \CCMS\Interfaces\Service\Factory\Model
	 * @autowire
	 */
	protected $modelFactory;

	/**
	 * @var \CCMS\Interfaces\Service\Auth
	 * @autowire
	 */
	protected $auth;

	/**
	 * @var \CCMS\Interfaces\Service\Repository
	 * @autowire
	 */
	protected $repository;

	/**
	 * @var User 
	 */
	protected $user;

	/**
	 * @param Route $route
	 * @return bool
	 * @throws Exception
	 */
	public function checkRoute(Route $route) {
		// Check, if route is default
		if ($route->getName() === static::DEFAULT_ROUTE) {
			return TRUE;
		}

		// Route not in ACL config? Redirect to Login
		$config = $this->ci->get(Service\Config::class)->get('acl');
		if (!in_array($route->getName(), array_keys($config))) {
			throw new Exception('Missing ACL config for route ' . $route->getName());
		}

		// Load ACL config for Route
		$routeAcl = $config[$route->getName()];

		// Full access
		if (empty($routeAcl)) {
			return TRUE;
		}

        // Any Logged in access
        if (in_array(true, $routeAcl, true)) {
            return TRUE;
        }

		// Check, if user has role required for route
		if ($this->auth->hasIdentity()) {
			/* @var $user User */
			$user = $this->repository->find($this->auth->getIdentity());

			foreach ($user->roles->models() as $role) {
				if (in_array($role->name, $routeAcl)) {
					return TRUE;
				}
			}
		}

		// Fallback, show default Route
		return FALSE;
	}

	/**
	 * @param Route $route
	 * @param Response $response
	 * @return Response
	 * @throws Exception
	 */
	public function checkRouteResponse(Route $route, Response $response) {
		return $this->checkRoute($route) ? $response : $response->withRedirect($this->ci->router->pathFor(static::DEFAULT_ROUTE));
	}

}
