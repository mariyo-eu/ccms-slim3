<?php

namespace CCMS\Actions;

use CCMS\Interfaces;
use Psr\Http\Message;

abstract class Base implements Interfaces\Action {

	/**
	 * @var \Psr\Http\Message\ServerRequestInterface
     * @autowire
	 */
	public $request;

	/**
	 * @var \Psr\Http\Message\ResponseInterface
     * @autowire
	 */
	public $response;

	/**
	 * Arguments passed from Router
	 * @var array
	 */
	public $args;

	/**
	 * @var \Slim\Router
	 * @autowire
	 */
	public $router;

	/**
	 * @var \CCMS\Interfaces\Renderer
	 * @autowire
	 */
	public $renderer;

	/**
	 * @var \CCMS\Interfaces\Service\Config
	 * @autowire
	 */
	public $config;

	/**
	 * @var \CCMS\Interfaces\Service\Flash
	 * @autowire
	 */
	public $flash;

	/**
	 * @var \CCMS\Interfaces\Service\Translator
	 * @autowire
	 */
	public $translator;

    /**
     * @var \CCMS\Interfaces\Service\Menu
     * @autowire
     */
    public $menu;

	/**
	 * @var string 
	 */
	protected $layout = '';

	/**
	 * Invoke controllers.
	 * @return string
	 */
	public function __invoke(Message\ServerRequestInterface $request, Message\ResponseInterface $response, $args) {
		// Args
		$this->args = $args;

		// Before Run
		$this->before();

		// Call action
		if ($this->response->getStatusCode() >= 200 and $this->response->getStatusCode() < 300) {
			$this->run();
		}

		// After Run
		$this->after();

		// Return response
		return $this->response;
	}

	/**
	 * Method before run()
	 */
	protected function before() {
		$this->renderer->addAttribute('action', $this);
	}

	/**
	 * Method after run()
	 */
	protected function after() {
		// Render layout
		$this->renderer->render($this->response, $this->layout);
	}

	/**
	 * Default service, when there is no set in dependencies.php
	 * @return Interfaces\Renderer
	 * @deprecated
	 */
	protected function serviceRenderer() {
		return $this->renderer;
	}

	/**
	 * @param   string  $string		text to translate
	 * @param   array   $values		values to replace in the translated text
	 * @param   int		$count		number for plural form
	 * @return  string
	 */
	public function _t($string, array $values = NULL, $count = FALSE) {
		// Context
		$context = str_replace('\\', '/', strtolower(get_called_class()));

		// Translate
		return $this->translator->_t($string, $context, $values, $count);
	}

	/**
	 * @param   array   $attributes		values to replace in the translated text
	 * @return  string
	 */
	protected function renderContent(array $attributes = []) {
		return $this->renderer->fetch(str_replace('\\', '/', strtolower(get_called_class())) . '.php', $attributes);
	}

	/**
	 * @return string
	 */
	public function __toString() {
		return $this->_t(get_called_class());
	}

	/**
	 * Title meta
	 * @return string
	 */
	public function getMetaTitle() {
		return (string) $this;
	}

}
