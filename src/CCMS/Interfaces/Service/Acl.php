<?php

namespace CCMS\Interfaces\Service;

use CCMS\Interfaces\Service;
use Slim\Route;
use Slim\Http\Response;
use Exception;

Interface Acl extends Service {

	/**
	 * @param Route $route
	 * @return bool
	 * @throws Exception
	 */
	public function checkRoute(Route $route);

	/**
	 * @param Route $route
	 * @param Response $response
	 * @return Response
	 * @throws Exception
	 */
	public function checkRouteResponse(Route $route, Response $response);
}
