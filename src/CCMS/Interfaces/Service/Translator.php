<?php

namespace CCMS\Interfaces\Service;

Interface Translator {

	/**
	 * @param	string   $string  text to translate
	 * @param	string   $context context of string
	 * @param	array    $values  values to replace in the translated text
	 * @param	int|bool $count   number for plural form
	 * @return	string
	 */
	public function _t($string, $context = NULL, array $values = NULL, $count = FALSE);
}
