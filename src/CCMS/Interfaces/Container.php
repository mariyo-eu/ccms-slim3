<?php

namespace CCMS\Interfaces;

use Psr\Container\ContainerInterface;

interface Container extends ContainerInterface
{
    public function create(string $class, array $params = []);
}
