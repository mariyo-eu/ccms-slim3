<?php

namespace CCMS\Interfaces;

Interface Model {

	/**
	 * Catching inside property call
	 * @param type $name
	 * @return bool
	 */
	public function hasProperty($name);

	/**
	 * Catching inside property call
	 * @param type $name
	 * @return type
	 */
	public function property($name);

	/**
	 * Set row.
	 * @param mixed $row
	 * @return $this
	 */
	public function set($row);

	/**
	 * Set value.
	 * @param mixed $value
	 * @return $this
	 */
	public function setValue($value);

	/**
	 * Get value.
	 * @return mixed
	 */
	public function getValue();

	/**
	 * Shortcut for Get value.
	 * @return mixed
	 */
	public function value();

	/**
	 * Set data.
	 * @param string $key
	 * @param mixed $value
	 * @return \CCMS\Interfaces\Model
	 */
	public function setData($key, $value);

	/**
	 * Get value from data.
	 * @return mixed
	 */
	public function getData($key = NULL);

	/**
	 * Get value from data.
	 * @return array
	 */
	public function getRouteData();

	/**
	 * Get primary key value from data.
	 * @return int
	 */
	public function pk();

	/**
	 * Model cleans after itself.
	 * @return \CCMS\Interfaces\Model
	 */
	public function cleanup();

	/**
	 * Model returns all property items.
	 * @return array
	 */
	public function getProperties();
}
