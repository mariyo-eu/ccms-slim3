<?php

use CCMS\Services;
use CCMS\Interfaces;
use Ohanzee\Helper\Arr;

// Session driver
$sessionParams = function(Interfaces\Container $ci) {
	$sessionDriver = isset($ci->get('settings')['session']['driver']) ? $ci->get('settings')['session']['driver'] : NULL;
	return [&$sessionDriver];
};

// PDO params
$pdoParams = function(Interfaces\Container $ci) {
	// Get config for Database
	$configDB = $ci->get(Interfaces\Service\Config::class)->get('database');

	return [
		$configDB['dsn'],
		$configDB['username'],
		$configDB['password'],
		Arr::get($configDB, 'options')
	];
};

// NotORM config
$notOrmParams = function(Interfaces\Container $ci) {
	return [$ci[PDO::class]];
};

$dependencies = [
	'router' => CCMS\Router::class,
	Interfaces\Container::class => $app->getContainer(),
	Interfaces\Service\Config::class => Services\Config::class,
	Interfaces\Service\Flash::class => [
		'class' => Services\Flash::class,
		'params' => $sessionParams
	],
	Interfaces\Service\Translator::class => Services\Translator::class,
	Interfaces\Service\Factory\Action::class => Services\Factory\Action::class,
	Interfaces\Service\Factory\Model::class => Services\Factory\Model::class,
	Interfaces\Service\Factory\Repository::class => Services\Factory\Repository::class,
	Interfaces\Renderer::class => [
		'class' => Services\Renderer::class,
		'params' => ['./protected/templates/']
	],
	PDO::class => [
		'class' => PDO::class,
		'params' => $pdoParams,
		'callback' => function(PDO $pdo) {
			$pdo->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES 'utf8mb4' COLLATE 'utf8mb4_unicode_ci'");
		}
	],
	NotORM::class => [
		'class' => NotORM::class,
		'params' => $notOrmParams
	],
	Interfaces\Service\Repository::class => [
		'class' => Services\Repository\Item::class,
		'factory' => Interfaces\Service\Factory\Repository::class
	],
	Interfaces\Service\Acl::class => Services\Acl::class
];

foreach ($dependencies as $dependencyKey => $dependencyOptions) {
	$class = is_array($dependencyOptions) ? $dependencyOptions['class'] : $dependencyOptions;
	$params = isset($dependencyOptions['params']) ? $dependencyOptions['params'] : [];
	$factory = isset($dependencyOptions['factory']) ? $dependencyOptions['factory'] : NULL;
	$callback = isset($dependencyOptions['callback']) ? $dependencyOptions['callback'] : NULL;
	\CCMS\registerServiceClosure($app, $dependencyKey, $class, $params, $factory, $callback);
}
